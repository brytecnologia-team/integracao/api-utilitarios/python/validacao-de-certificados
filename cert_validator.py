import requests
import json
import base64
import time

URL_BASE = "https://fw2.bry.com.br"
#https://URL_HUB_SIGNER

VALIDATOR_URL_SERVER = URL_BASE + "/api/validador-service/v1/certificateReports"

HEADER = {'Content-Type': 'application/json', 'Authorization': 'Bearer insert-a-valid-token'}

# Step 1 - Load the certificate to be validated
certificate = open("./certificate/cert.cer", 'rb').read()
b64_certificate = base64.b64encode(certificate)
b64_certificate_str = b64_certificate.decode("utf-8") 

def token_verify():
    auxAuthorization = HEADER['Authorization'].split(' ')
    if(auxAuthorization[1] == 'insert-a-valid-token'):
        print("Configure a valid token")
        return False
    else:
        return True
        



def cert_validate():

    print("================ Initializing certificate verification ... ================")
    print("")

    verification_form = {
        'nonce': 1,
        'mode': 'CHAIN',
        'contentsReturn': 'true',
        'extensionsReturn': 'true',
        "certificates":[{"nonce":1,"content": b64_certificate_str}]
    }
    # Step 2 - Send a request to the BRy service and print all the values ​​related to the certificate and its validity.
    response = requests.post(VALIDATOR_URL_SERVER, data = json.dumps(verification_form), headers=HEADER)
    if response.status_code == 200:
        data = response.json()

        chain_status = data['reports'][0]['status']
        cert_index = len(chain_status['certificateStatusList']) - 1
        holder_certificate = chain_status['certificateStatusList'][cert_index]

        print("JSON response from the validator: ", data)
        print("")
        print("Holder informations: ")
        if chain_status != None and holder_certificate != None:
            print("Nome: ", holder_certificate['certificateInfo']['subjectDN']['cn'])
            print('General status of the certificate chain: ', chain_status['status'])
            print('Certificate status: ', holder_certificate['status'])
            if holder_certificate['status'] == "INVALID":
                print(holder_certificate['errorStatus'])
                if holder_certificate['revocationStatus'] != None:
                    print(holder_certificate['revocationStatus'])
            print('iCP-Brazil certificate: ', holder_certificate['pkiBrazil'])
            print('Initial certificate expiration date: ', holder_certificate['certificateInfo']['validity']['notBefore'])
            print('Final certificate expiration date: ', holder_certificate['certificateInfo']['validity']['notAfter'])
        
        else:
            print('Incomplete chain of the signatory: It was not possible to verify the certificate')
            print('Overall chain status ', chain_status['status'])

    else:
        print(response.text)


if(token_verify()):
    cert_validate()

